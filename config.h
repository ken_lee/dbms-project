#ifndef  _CONFIG_H
#define  _CONFIG_H

#define _CRT_SECURE_NO_WARNINGS
#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<iostream>
#include<time.h>
#include<string>
#include <conio.h>

const int buffer=64;//buffer size: 64 Pages
const int pageSize=256;//page size: 64KB (256 records)

struct orderData{// 256B
	int orderKey;//1~6,000,000   4B
	int custKey;//max 6bit       4B
	char orderStatus;//O or F   1B
	double totalPrice;//        8B
	char orderDate[11];//xxxx-xx-xx 11B
	char orderPriority[16];//max: "x-NOT SPECIFIED"   16B
	char clerk[16];//Clerk#xxxxxxxxx                16B
	int shipPriority;//4B
	char comment[176];
};
enum KeyCmp {
	ORDERKEY, CUSTKEY, ORDERDATE
};
#endif //_CONFIG_H