#ifndef  _QUICK_INTERNAL_SORT_H
#define  _QUICK_INTERNAL_SORT_H
//! sherlock DBMS_Group
// !TODO wirte our own internalsort Sherlock
#include "config.h"

//  our own internal sort entry
void quickInternalSort(orderData *arr, int size, KeyCmp keyCmp = ORDERKEY);
//  actual function
void quickInternalSort(orderData *arr, int size, bool (*cmp)(const orderData &a, const orderData &b));
//	cmp functions
bool keyCmp_OrderKey(const orderData &a, const orderData &b);
bool keyCmp_CustKey(const orderData &a, const orderData &b);
bool keyCmp_OrderDate(const orderData &a, const orderData &b);
bool cmp(const orderData &a, const orderData &b,KeyCmp keyCmp);
#endif //_QUICK_INTERNAL_SORT_H
