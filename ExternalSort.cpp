/*ALL Copyright Reversed by KenLee@2014*/
#include "ExternalSort.h"

orderData *front100 = new orderData[100];

void printFront100(FILE *file){
	fread(front100,sizeof(orderData),100,file);
	for (int i=0;i<100;i++)
	{
		cout<<front100[i].orderKey<<"  ";
		cout<<front100[i].custKey<<"  ";
		cout<<front100[i].orderStatus<<"  ";
		cout<<front100[i].totalPrice<<"  ";
		cout<<front100[i].orderDate<<"  ";
		cout<<front100[i].orderPriority<<"  ";
		cout<<front100[i].clerk<<"  ";
		cout<<front100[i].shipPriority<<"  ";
		cout<<front100[i].comment<<"  ";
		cout<<endl;
	}
	return;
}
//split origin file to allrecords / b files write it by binary way
int externalSortInit(FILE *originFile, KeyCmp keyCmp){
	orderData *memory = new orderData[pageSize*buffer];// All memory
	char temp[256];
	int pos=0;
	int tempCounter=0;
	FILE *outFile;
	int fileCounter=0;
	char outFileName[100];
	int recordCounter=0;
	//cout<<"system call: mkdir 0th-operation"<<endl;
	system("mkdir 0th-operation");
	sprintf(outFileName,"0th-operation\\%d.tbl",fileCounter);
	outFile=fopen(outFileName,"wb");
	while (fgets(temp,500,originFile)!=NULL)
	{
		split(temp,&memory[pos]);
		pos++;
		if(pos>=pageSize*buffer){
			quickInternalSort(memory,pageSize*buffer,keyCmp);
			fwrite(memory,sizeof(orderData),pageSize*buffer,outFile);
			fclose(outFile);
			fileCounter++;
			sprintf(outFileName,"0th-operation\\%d.tbl",fileCounter);
			outFile=fopen(outFileName,"wb");
			pos=0;
		}
	}
	if(pos != 0){
		quickInternalSort(memory, pos, keyCmp);
		fwrite(memory,sizeof(orderData),pos,outFile);
	}
	//cout<<"Complete init "<<fileCounter+1<<" files in "<<clock()/1000.0<<" secs!"<<endl;
	delete[] memory; memory = NULL;
	fclose(outFile);
	return fileCounter;
}

// dataBuffer constructor
dataBuffer::dataBuffer(int size) {
	if (size % 4 != 0) {
		size = size - size % 4 + 4;  // make "size" can be divided by four
	}
	this->size = size;
	arrayA = (orderData*) malloc(sizeof(orderData) * size / 4);
	arrayB = (orderData*) malloc(sizeof(orderData) * size / 4);
	sorted = (orderData*) malloc(sizeof(orderData) * size / 2);
	posA = 0;
	posB = 0;
	sizeA = 0;
	sizeB = 0;
	sortedSize = 0;
	return;
}

// dataBuffer destructor
dataBuffer::~dataBuffer() {
	if (arrayA != NULL) {
		free(arrayA); arrayA = NULL;
	}
	if (arrayB != NULL) {
		free(arrayB); arrayB = NULL;
	}
	if (sorted != NULL) {
		free(sorted); sorted = NULL;
	}
}

// merge funtion for a buffer
int myMerge(dataBuffer &buff, KeyCmp keyCmp) {
// return: 0(normal) 1(one exhausted) 2(both exhausted) -1(error)
	buff.sortedSize = 0;
	////int maxSize = buff.size / 4;
	// myMerge merges part of arrayA and arrayB until one of them is exhausted
	// update the size (B or A) and the sortedSize
	if (buff.sizeA != 0 && buff.sizeB != 0) {
		while (buff.posA < buff.sizeA && buff.posB < buff.sizeB) {
			if (cmp(buff.arrayA[buff.posA], buff.arrayB[buff.posB], keyCmp)) {  // a[A] < b[B]
				buff.sorted[buff.sortedSize] = buff.arrayA[buff.posA];
				buff.posA++;
			} else {
				buff.sorted[buff.sortedSize] = buff.arrayB[buff.posB];
				buff.posB++;
			}
			buff.sortedSize++;
		}
		return 0;
	}
	// if one of them is already exhausted, just patch it
	if (buff.sizeA != 0 && buff.sizeB == 0) {  // B exhausted
		while (buff.posA < buff.sizeA) {
			buff.sorted[buff.sortedSize] = buff.arrayA[buff.posA];
			buff.posA++;
			buff.sortedSize++;
		}
		return 1;
	} else if (buff.sizeA == 0 && buff.sizeB != 0) {  // A exhausted
		while (buff.posB < buff.sizeB) {
			buff.sorted[buff.sortedSize] = buff.arrayB[buff.posB];
			buff.posB++;
			buff.sortedSize++;
		}
		return 1;
	}
	// if both of them are exhausted, do nothing
	if (buff.sizeA == 0 && buff.sizeB == 0) {
		return 2;
	}
	return -1;  // if error occurs
}

// merge funtion for files (the main merge function)
int myMerge(FILE *&a, FILE *&b, FILE *&sorted, KeyCmp keyCmp) {
	dataBuffer buff(pageSize * buffer);
	int maxSize = buff.size / 4;
	// initialize buffer
	buff.sizeA = fread(buff.arrayA, sizeof(orderData), maxSize, a);
	buff.sizeB = fread(buff.arrayB, sizeof(orderData), maxSize, b);
	// start merging with aid of dataBuffer
	while (!(buff.sizeA == 0 && buff.sizeB == 0)) {
		buff.sortedSize = 0;
		if (myMerge(buff, keyCmp) == 2) break;
		fwrite(buff.sorted, sizeof(orderData), buff.sortedSize, sorted);
		if (buff.posA == buff.sizeA) {  // A exhausted
			buff.sizeA = fread(buff.arrayA, sizeof(orderData), maxSize, a);
			buff.posA = 0;
		}
		if (buff.posB == buff.sizeB) {  // B exhausted
			buff.sizeB = fread(buff.arrayB, sizeof(orderData), maxSize, b);
			buff.posB = 0;
		}
	}
	return 0;
}

// 
int externalSort(int initedFilesNum,KeyCmp keyCmp){
	int filesCounter=initedFilesNum;
	int times=1;
	int limit;
	char mkCommand[100];
	char readFilePathA[50];
	char writeFilePath[50];
	char readFilePathB[50];
	while(filesCounter+1>1){
		int readFileCounter=0;
		int writeFileCounter=0;
		FILE *a,*b,*o;
		sprintf(mkCommand,"mkdir %dth-operation",times);
		//cout<<"system call: "<<mkCommand<<endl;
		system(mkCommand);
		if((filesCounter+1)%2==0){
			limit=filesCounter;
			filesCounter=0;
		}
		else
		{
			limit=filesCounter-1;
			sprintf(mkCommand,"copy %dth-operation\\%d.tbl %dth-operation\\%d.tbl",times-1,filesCounter,times,filesCounter/2);
			//cout<<"system call: "<<mkCommand<<endl;
			filesCounter=1;
			system(mkCommand);
		}
		while(readFileCounter<limit){
			sprintf(readFilePathA,"%dth-operation\\%d.tbl",times-1,readFileCounter);
			readFileCounter++;
			sprintf(readFilePathB,"%dth-operation\\%d.tbl",times-1,readFileCounter);
			readFileCounter++;
			sprintf(writeFilePath,"%dth-operation\\%d.tbl",times,writeFileCounter);
			writeFileCounter++;
			o=fopen(writeFilePath,"wb");
			a=fopen(readFilePathA,"rb");
			b=fopen(readFilePathB,"rb");
			myMerge(a, b, o, keyCmp);
			fclose(a);
			fclose(b);
			fclose(o);
		}
		cout<<"done "<<times<<" operation. time_until_now:"<<clock()/1000.0<<"secs!"<<endl;
		filesCounter=writeFileCounter-1+filesCounter;
		times++;
	}
	return times-1;
}
