//! sherlock DBMS_Group
#include "QuickInternalSort.h"
using namespace std;
bool cmp(const orderData &a, const orderData &b,KeyCmp keyCmp){
	switch (keyCmp) {
	case ORDERKEY:
		return keyCmp_OrderKey(a,b);
		break;
	case CUSTKEY:
		return keyCmp_CustKey(a,b);
		break;
	case ORDERDATE:
		return keyCmp_OrderDate(a,b);
		break;
	}
}
inline bool keyCmp_OrderKey(const orderData &a, const orderData &b) {
	return a.orderKey <= b.orderKey ? true : false;
}

inline bool keyCmp_CustKey(const orderData &a, const orderData &b) {
	return a.custKey <= b.custKey ? true : false;
}

inline bool keyCmp_OrderDate(const orderData &a, const orderData &b) {
	for (int i = 0; i <= 9; i++) {
		if (i == 4 || i == 7) continue;  // skip the character '-'
		if (a.orderDate[i] < b.orderDate[i]) return true;
		else if (a.orderDate[i] > b.orderDate[i]) return false;
		else continue;
	}
	return true;
}

void quickInternalSort(orderData *arr, int size, KeyCmp keyCmp){
	// the internal sort returns an ascending version
	// default key: ORDERKEY
	switch (keyCmp) {
	case ORDERKEY:
		quickInternalSort(arr, size, keyCmp_OrderKey);
		break;
	case CUSTKEY:
		quickInternalSort(arr, size, keyCmp_CustKey);
		break;
	case ORDERDATE:
		quickInternalSort(arr, size, keyCmp_OrderDate);
		break;
	}
	return;
}

void quickInternalSort(orderData *arr, int size, 
					   bool (*cmp) (const orderData &a, const orderData &b)) {
// if cmp(a, b) returns true, a is prior than b
// function sorts arr in a prior sequence

	// special case 0: size errors
	if (size <= 0) return;  // should throw an exception
	
	// special case 1: one element only
	if (size == 1) return;  // no sort
	
	// special case 2: two elements
	if (size == 2) {
		if (cmp(arr[0], arr[1]) == false) {
			swap(arr[0], arr[1]);
		}
		return;
	}
	
	// general cases:
	
	// choose key and swap it to last
	int key = rand() % size;
	swap(arr[size-1], arr[key]);
	key = size - 1;
	
	// compare and depart
	int left = 0, right = size - 2;
	while (left != right) {
		if (cmp(arr[left], arr[key]) == false) {
			// if arr[left] is not prior than arr[key]
			swap(arr[left], arr[right]);
			right--;
		} else {  // left prior than key
			left++;
		}
	}
	
	// swap key to the middle
	if (cmp(arr[left], arr[key]) == true) {
		// if arr[left] is prior than arr[key]
		swap(arr[left + 1], arr[key]);
		key = left + 1;
	} else {
		swap(arr[left], arr[key]);
		key = left;
	}
	
	// recurrence sort of the two parts
	quickInternalSort(arr, key, cmp);
	quickInternalSort(&arr[key + 1], size - key - 1, cmp);
	return; 
}

