/*ALL Copyright Reversed by KenLee@2014*/
#ifndef  _EXTERNAL_SORT_H
#define  _EXTERNAL_SORT_H
#include "QuickInternalSort.h"
#include "StringHandler.h"
using namespace std;

//split origin file to allrecords / b files ,sort it and write it by binary way
int externalSortInit(FILE *originFile,KeyCmp keyCmp);

//externalSort using existed inited files
int externalSort(int initedFilesNum,KeyCmp keyCmp);

//merge function
struct dataBuffer {
	int size;  // total buffer size
	orderData *arrayA;
	orderData *arrayB;
	orderData *sorted;
	int posA;	// the recent position of arrayA
	int posB;	// the recent position of arrayB
	int sizeA;	// elements of arrayA
	int sizeB;  // elements of arrayB
	int sortedSize;  // the position of sorted_size
	dataBuffer(int size);  // constructor
	~dataBuffer();  // destructor
};
int myMerge(dataBuffer *buff, KeyCmp keyCmp);  // for buffer
int myMerge(FILE *&a, FILE *&b, FILE *&sorted, KeyCmp keyCmp);  // for files

//for debug
void printFront100(FILE *file);
#endif //_EXTERNAL_SORT_H
