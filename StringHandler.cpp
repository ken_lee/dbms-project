//! DBMS_Group lyc
#include "StringHandler.h"
// string to double or int
// input string para,output a double
double mySton(string para){
  double result = 0.0;
  int i = 0;// the position of para's character
  double factor = 0.1;// uses in decimal part

  // integer part
  while(para[i] != '\0' && para[i] != '.') {
    result = result * 10.0 + (para[i]-'0') * 1.0;
    i++; 
  }
  
  // decimal part
  if(para[i]=='.') {
	  i++;//move to first bit of decimal
      while(para[i] != '\0') { 
        result += (para[i] - '0') * factor;
        i++;
        factor /= 10.0;
	  }
  }

  return result;
}

// string to char array
// input string para,output arr
void myStrToChar(char *arr, string para){
  int i = 0;
  while(para[i] != '\0') {
    arr[i] = para[i];
    i++;
  }
  arr[i] = '\0'; 
}

// the spilt function take char array for input and a struct orderDate for output
// array str is input,temp is output
void split(char *str, orderData* temp){
  string buffer[9];// contain each part of record
  int position = 0;
  int number = 0;// the number of buffer

  // divide record 
  while(1) {
    if(str[position] == '|'  ) {
      buffer[number] += '\0';
      number++;
      if(number == 9) break;
    } else {
      buffer[number] += str[position];
    }
    position++;
  }
 
  // transfer data form buffer to orderDate
  temp -> orderKey = mySton(buffer[0]);
  temp -> custKey = mySton(buffer[1]);
  temp -> orderStatus = buffer[2][0];
  temp -> totalPrice = mySton(buffer[3]);
  myStrToChar(temp -> orderDate,buffer[4]);
  myStrToChar(temp -> orderPriority,buffer[5]);
  myStrToChar(temp -> clerk,buffer[6]);
  temp -> shipPriority = mySton(buffer[7]); 
  myStrToChar(temp -> comment,buffer[8]); 
}

