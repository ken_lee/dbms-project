#ifndef  _STRING_HANDLER_H
#define  _STRING_HANDLER_H
#include "config.h"
using namespace std;
//string to double or int ,you can replace by other function like atoi() or atof()
double mySton(string para);
//string to char array
void myStrToChar(char *arr,string para);
//the spilt function take char array for input and a struct orderDate for output
void split(char *str,orderData* temp);
#endif //_STRING_HANDLER_H